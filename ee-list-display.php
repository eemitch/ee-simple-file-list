<?php // Simple File List - ee-list-display.php - v10.17.15 - mitchellbennis@gmail.com
	
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if ( ! wp_verify_nonce( $nonce1, 'ee_main_page_display' ) ) exit; // Exit if nonce fails
	
// If Delete Files...
if(@$_POST['eeListForm']) {

	foreach($_POST['eeDeleteFile'] as $eeKey => $eeFile){
		
		if(unlink($eeUploadDir . $eeFile)) {
			$eeMsg = 'Deleted the file &rarr; ' . $eeFile;
			$eeLog[] = $eeMsg;
			$eeMessages[] = $eeMsg;
		} else {
			$eeMsg = 'Could not delete the file: ' . $eeFile;
			$eeLog[] = $eeMsg;
			$eeMessages[] = $eeMsg;
		}
	}
}

if($eeMessages) { 
	echo '<div id="eeMessaging" class="updated">';
	eeMessageDisplay($eeMessages);
	echo '</div>';
} ?>

<?php if($eeErrors) { 
	echo '<div id="eeMessaging" class="error">';
	eeMessageDisplay($eeMessages);
	echo '</div>';
}

// List files in folder, add to array.
if ($eeHandle = @opendir($eeUploadDir)) {

	while (false !== ($eeFile = readdir($eeHandle))) {
		if (!@in_array($eeFile, $eeExcluded)) {
			if (!@is_dir($eeFile)) {
				$eeFiles[] = $eeFile; // Add the file to the array
			}	
		}
	}
	@closedir($eeHandle);
	
	
	// Sort Array
	@natcasesort($eeFiles);
	
	$eeFileCount = count($eeFiles);
	
	if($eeFileCount) {
		
		$eeMsg = "There are " . $eeFileCount . " files in the list";
		
		$eeLog[] = $eeMsg;
		
		if($eeAdmin) {
		
		?>
		
	<form action="<?php echo $_SERVER['PHP_SELF'] . '?page=ee-simple-file-list'; ?>" method="post">	
		<input type="submit" name="eeListForm" value="Delete Checked" class="alignright" />
		
	<?php } ?>
	
	<p><?php echo $eeMsg; ?></p>
	
	<table class="eeFiles">
		<tr>
			<th>File</th>
			<th>Size</th>
			<th>Date</th>
			<?php if($eeAdmin) { echo "<th>Delete</th>"; } ?>
		</tr>
		
		<?php
		
		// Loop through array
		foreach($eeFiles as $eeKey => $eeFile) {
			
			echo '<tr>';
			
			// File Name
			echo '<td><a href="' . $eeUploadLink . $eeFile .  '" target="_blank">'. $eeFile . '</a></td>';
			
			// File Size
			echo '<td>' . eeBytesToSize(filesize($eeUploadDir . $eeFile)) . '</td>';
			
			// File Modification Date
			echo '<td>' .  date('d-m-Y' ,@filemtime($eeUploadDir . $eeFile)). '</td>';
			
			if($eeAdmin) {
				echo '<td><input type="checkbox" name="eeDeleteFile[]" value="' . $eeFile . '" /></td>';
			}
			
			echo '</tr>';		
			
		} // END loop
	
		echo '</table>';
	 
	 }
	
	if($eeAdmin) { echo "</form>"; }

} else {

	$eeLog[] = 'Error: Can\'t read the files in the Uploads folder. ';
}

if (count($eeFiles) < 1) {
	echo "<p>There are no files to list.</p>";
}

?>