<?php
/**
 * @package Element Engage - Simple File List
 * @version 1.0
 * No License Restrictions - Use and Modify as You Wish - No Warranty
 */
/*
Plugin Name: Simple File List
Plugin URI: http://elementengage.com/ee-simple-file-list/
Description: Simple File List is an ultra-basic front-side uploader with optional file list built for Wordpress designers and developers.
Author: Mitchell Bennis - Element Engage, LLC
Version: 1.0 - 10.15.15
Author URI: http://elementengage.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $wpdb;
if(!$wpdb) { exit('No DB'); }

// === Configuration ==================================================

$eeDevMode = FALSE; // Set false for normal use
$eeLogFile = FALSE; // Set false to not log things
// The log file is written to the uploads folder and visible in the file list.

// Admin Titling
$eePageTitle = 'Simple File List Administration';
$eeMenuLabel = 'Simple File List';
$eeUploadFolderName = 'simple-file-list'; // No spaces

// Upload Vars
$eeAllowList = ''; // Show the file list
$eeFormats = '';
$eeAllowUploads = ''; // Show the upload form		
$ee_max_file_size = 1; // MB
$ee_post_max_size = 1;

// Variable Setup
$eeExcluded = array('.', '..', "", basename($_SERVER['PHP_SELF']), '.htaccess', '.ftpquota', 'error_log'); // Exclude these files
$eeUploadDirArray = wp_upload_dir();
$eeUploadDir = $eeUploadDirArray['basedir'] . '/' . $eeUploadFolderName . '/';
$eeUploadLink = $eeUploadDirArray['baseurl'] . '/' . $eeUploadFolderName . '/';

// Email
$eeAdminTo = get_option('admin_email');
$eeAdminFrom = get_option('admin_email');

// Initialization
$eeErrors_email = $eeAdminTo;
$eeBasePath = $_SERVER['DOCUMENT_ROOT'];
$eeUserAccess = 'edit_posts'; // Wordpress user level.
$eePluginPath = plugin_dir_path( __FILE__ );
$eePluginURL = plugin_dir_url( __FILE__ ); // <<<------- Removed hard coded folder name 11.17.15
$eeMessages = array();
$eeMsg = '';
$eeErrors = array();
$eeLog = array();
$eeAdmin = FALSE;

$eeBackLink = 'http://elementengage.com';
$eeBackLinkTitle = 'Script by Element Engage Media Works';
$eeDisclaimer = 'IMPORTANT - Allowing the public to upload files to your web server comes with risk. Please double-check that you only use the file types you absolutely need and open each file submitted to you with great caution and intestinal fortitude.';

// Let's go! -----------------------------------------------------------

$eeLog[] = 'Simple File List is Running!';

// Are we in the admin area?
if(strpos($_SERVER['PHP_SELF'], 'wp-admin')) {
	$eeAdmin = TRUE;
}

function eeActivate() {
	
	global $wpdb, $eeLog;
	
	// Check if options exist in the database
	if($wpdb->query("SELECT option_name FROM " . $wpdb->options . " WHERE option_name = 'eeSFL'") != 1) {
		
		$eeLog[] = 'Database Installation...';
		
		// These are the default values set at the top of the script, not user input
		$eeSettings = 'eeAllowList=Yes|';
		$eeSettings .= 'eeAllowUploads=Yes|';
		$eeSettings .= 'ee_upload_max_filesize=10|';
		$eeSettings .= 'eeFormats=gif, jpg, jpeg, bmp, png, tif, tiff, txt, eps, psd, ai, pdf, doc, xls, ppt, docx, xlsx, pptx, odt, ods, odp, odg, wav, wmv, wma, flv, 3gp, avi, mov, mp4, m4v, mp3, webm, zip|';
		$eeSettings .= 'eeAdminTo=' . get_option('admin_email');
		
		// Add the new option_names
		$eeQuery = "INSERT INTO " . $wpdb->options . " (option_name, option_value) VALUES ('eeSFL', '$eeSettings')";
							
		if($wpdb->query($eeQuery)) {
			$eeLog[] = 'New Record Added to the Database';
		} else {
			$eeLog[] = 'ERROR - Could not create database record.';
		}
	
	} // ENDs install check

	// Write the upload folder
	eeUploadDirCheck();
}

register_activation_hook( __FILE__, 'eeActivate' );


// Detect max upload size.
function eeDetectUploadLimit() {
	
	global $ee_max_file_size, $ee_post_max_size;
	
	$ee_upload_max_upload_size = substr(ini_get('upload_max_filesize'), 0, -1); // Strip off the "M".
	$ee_post_max_size = substr(ini_get('post_max_size'), 0, -1); // Strip off the "M".
	if ($ee_upload_max_upload_size <= $ee_post_max_size) { // Check which is smaller, upload size or post size.
		$ee_max_file_size = $ee_upload_max_upload_size;
	} else {
		$ee_max_file_size = $ee_post_max_size;
	}
}
eeDetectUploadLimit();


// Load stuff we need in the head
function ee_custom_admin_head() {

	global $eePluginURL;	
	
	echo '<link rel="stylesheet" type="text/css" href="' . $eePluginURL . 'css/eeStyling.css">';
	// wp_enqueue_script('eeMainJS', $eePluginURL . 'scripts/eeMain.js');

}
add_action('admin_head', 'ee_custom_admin_head');


function eeEnqueue() {
	
	global $eePluginURL;
	
	// Register the style like this for a theme:
    wp_register_style( 'ee-plugin-css', $eePluginURL . 'css/eeStyling.css');
 
    // Enqueue the style:
    wp_enqueue_style('ee-plugin-css');
	
	// wp_enqueue_script('eeMainJS', $eePluginURL . 'scripts/eeMain.js');
}
add_action( 'wp_enqueue_scripts', 'eeEnqueue' );





// === FUNCTIONS ===================================


function eeUploadDirCheck() {
	
	global $eeUploadDir, $eeLog, $eeErrors;
	
	if(!@is_writable($eeUploadDir)) {
		$eeLog[] = 'No Upload Directory Found.';
		$eeLog[] = 'Creating Upload Directory ...';
		
		if(!@mkdir($eeUploadDir, 0755)) {
			$message = 'ERROR - Could not CREATE the upload directory: ' . $eeUploadDir;
			$eeLog[] = $message;
			$eeErrors = $message;
			
			return FALSE;
		
		} else {
			
			if(!@is_writable($eeUploadDir)) {
				$message = 'ERROR - Could not READ the new upload directory: ' . $eeUploadDir;
				$eeLog[] = $message;
				$eeErrors = $message;
			} else {
				return TRUE;
			}
		}
	} else {
		$eeLog[] = 'Upload Folder: ' . $eeUploadDir;
		return TRUE;
	}
}



// Show the latest post
function eeSFLshortcode( $atts ) {
    
    // Usage: [eeSFL]
    ee_main_page_display(); 
}
add_shortcode( 'eeSFL', 'eeSFLshortcode' );



// Make sure user can access this stuff
function eeCheckUser() {
	global $eeUserAccess;
	require_once(ABSPATH . "wp-includes/pluggable.php");
	if (current_user_can($eeUserAccess)) {
		return TRUE;
	} else {
		return FALSE;
	}	
}


// Problem Display / Error reporting
function eeMessageDisplay($eeMsg) {
	
	global $eeLog;
	
	$eeLog[] = 'Displaying User Messages';
	$eeLog[] = $eeMsg;
	
	if(is_array($eeMsg)) {
		echo '<div id="eeMessageDisplay"><ul>'; // Loop through $eeMessages array
		foreach($eeMsg as $key => $value) { 
			if(is_array($value)) {
				foreach ($value as $value2) {
					echo "<li>$value2</li>\n";
				}
			} else {
				echo "<li>$value</li>\n";
			}
		}
		echo "</ul></div>\n\n";
	} else {
		$eeLog[] = 'Bad Array';
		return FALSE;
	}

}

// EMAIL NOTIFICATION
function eeNoticeEmail($eeMsg, $eeMsgMode = 'standard') {

	global $eeAdminTo;
	
	$eeAdminFrom = $eeAdminTo;
	
	if($eeAdminTo) { // Added TO address requirement 11.17.15
		
		$body = '';
		$headers = "From: $eeAdminFrom\nReturn-Path: $eeAdminFrom\nReply-To: $eeAdminFrom\n";
		
		if($eeMsgMode == 'error') {
			$subject = "Simple File List Error";
		} else {
			$subject = "Simple File List Admin Notice";
		}
		
		if(is_array($eeMsg)) {
			foreach ($eeMsg as $value) {
				if(is_array($value)) {
					foreach ($value as $value2) {
						$body .= $value2 . "\n\n";
					}
				} else {
					$body .= $value . "\n\n";
				}
			}
		} else {
			$body = $eeMsg . "\n\n";
		}
		
		$body .= 'Via: ' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
	
		mail($eeAdminTo,$subject,$body,$headers); // Email the message or error report
	}
	
	return FALSE;		
}

// Size formatting
function eeBytesToSize($bytes, $precision = 2) {  
    
    $kilobyte = 1024;
    $megabyte = $kilobyte * 1024;
    $gigabyte = $megabyte * 1024;
    $terabyte = $gigabyte * 1024;
   
    if (($bytes >= 0) && ($bytes < $kilobyte)) {
        return $bytes . ' B';
 
    } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
        return round($bytes / $kilobyte, $precision) . ' KB';
 
    } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
        return round($bytes / $megabyte, $precision) . ' MB';
 
    } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
        return round($bytes / $gigabyte, $precision) . ' GB';
 
    } elseif ($bytes >= $terabyte) {
        return round($bytes / $terabyte, $precision) . ' TB';
    } else {
        return $bytes . ' B';
    }
}


// Image Uploading - Note that no upload or file data is stored in the database.
function eeUpload($eeFormats) {
	
	global $eeAdmin, $eeUploadDir, $eeUploadLink, $eeErrors, $eeLog;
	
	// Get the original file extension
	$fileName = strtolower(basename($_FILES['eefile']['name']));  // Read original name of the uploaded file.
	$ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
	
	// Remove white space from array values
	$eeFormatsArray = array_map('trim', explode(',', $eeFormats));
	
	// Only allow allowed files, ah?
	if (in_array($ext,$eeFormatsArray)) {
	
		$eeLog[] = 'Beginning the upload...';
		
		// File Naming
		$fileName = pathinfo($_FILES['eefile']['name'], PATHINFO_FILENAME);  
		$time = date('m-d-Y--G:i:s');
		$fileName = str_replace(' ', '_', $fileName); // Replace any spaces with underscores
		$fileName = preg_replace("/[^a-z0-9._-]/i", "", $fileName); // Get rid of problem characters for the file name.
	
		$newFile = $fileName . "." . $ext;  // Assemble new file name and extension.
		$targetPath = $eeUploadDir . basename($newFile); // Define where the file will go.
	
		if (@move_uploaded_file($_FILES['eefile']['tmp_name'], $targetPath)) { // Move the file to the final destination
				
			$message = "File Uploaded...\n\n" . $eeUploadLink . $newFile . " \n\n(" . eeBytesToSize(filesize($eeUploadDir . $newFile)) . ')';
			$eeMessages[] = $message;
			$eeLog[] = $message;
			eeNoticeEmail($message);
			
			if(!$eeAdmin) {
				?><script>alert('File Upload Complete');</script><?php
			}
			
			return TRUE;
		
		} else { // Upload Problem
			$eeErrors[] = 'No file was uploaded';
			
			switch ($_FILES['eefile']['error']) {
				case 1:
					// The file exceeds the upload_max_filesize setting in php.ini
					$eeErrors[] = 'File Too Large - Please resize your file to meet the file size limit.';
					break;
				case 2:
					// The file exceeds the MAX_FILE_SIZE setting in the HTML form
					$eeErrors[] = 'File Too Large - Please resize your file to meet the file size limits.';
					break;
				case 3:
					// The file was only partially uploaded
					$eeErrors[] = 'Upload Interrupted - Please back up and try again.';
					break;
				case 4:
					// No file was uploaded
					$eeErrors[] = 'No File was Uploaded - Please back up and try again.';
					break;
			}
			
			?><script>alert('File Upload FAILED!');</script><?php
			
		}
	} else {
		$eeErrors[] = 'Sorry, the file type being uploaded is not accepted by this website.';
		$eeErrors[] = "Filetype: $ext";
		
	}
	
	$eeLog[] = $eeErrors;
	eeNoticeEmail($eeErrors, 'errors');
	
	return FALSE;
			
} // END File Upload 

// END FUNCTIONS ---------------


// === ADMIN PAGE SETUP ========================================================

// Register the function using the admin_menu action hook.
add_action( 'admin_menu', 'ee_plugin_menu' );

// Create a function that contains the menu-building code.
function ee_plugin_menu() {
	global $eePageTitle, $eeMenuLabel, $eeUserAccess;
	add_menu_page( $eePageTitle, $eeMenuLabel, $eeUserAccess, 'ee-simple-file-list', 'ee_main_page_display', '', 4);
	add_submenu_page( 'ee-simple-file-list', $eePageTitle, 'Settings', 'edit_users', 'ee-simple-file-list-admin', 'ee_admin_page_display');
}

// Create the HTML output for the page (screen) displayed when the menu item is clicked.
function ee_main_page_display() {
	
	eeCheckUser();
	global $wpdb, $eeLog, $eeDevMode, $eeErrors, $eeMessages, $eeMenuLabel, $eeLogFile, 
		$eePluginPath, $eeUploadDir, $eeUploadLink, $eeAdmin;
		
	// Create some nonces to check on the included pages.
	$nonce1 = wp_create_nonce('ee_main_page_display');
	$nonce2 = wp_create_nonce('ee_upload_display');
	
	// Get current info...
	$eeQuery = "SELECT option_value FROM " . $wpdb->options . " WHERE option_name = 'eeSFL'";
	
	// Run the query
	$eeResult = $wpdb->get_results($eeQuery, ARRAY_N);
	
	if($eeResult) {
		// Get the result
		$eeSettings = $eeResult[0][0];
		$eeLog[] = $eeSettings;
		$eeSettings = explode('|', $eeSettings);
		
		// Set our variables
		$eeAllowList = explode('=', $eeSettings[0]);
		$eeAllowList = $eeAllowList[1];
		
		$eeAllowUploads = explode('=', $eeSettings[1]);
		$eeAllowUploads = $eeAllowUploads[1];
		
		$ee_upload_max_filesize = explode('=', $eeSettings[2]);
		$ee_upload_max_filesize = $ee_upload_max_filesize[1];
		
		$eeFormats = explode('=', $eeSettings[3]);
		$eeFormats = $eeFormats[1];
		
		$eeAdminTo = explode('=', $eeSettings[4]);
		$eeAdminTo = $eeAdminTo[1];
		
		// Check for upload POST and Nonce
		if(@$_POST['eeUpload'] AND check_admin_referer( 'ee-simple-file-list-upload', 'ee-simple-file-list-upload-nonce')) {
			
			// Here where we upload the file
			if(eeUpload($eeFormats)) {
				$eeMessages[] = "File Upload Complete";	
			}		
		} else {
			$eeLog[] = 'No File Upload';
		}
	}
	
	
	echo '<div id="eeSFL"';
	if($eeAdmin) { echo 'class="eeAdminEntry"'; }
	echo '>';
	
	// Front Side Display
	if(!$eeAdmin AND $eeAllowUploads == 'Yes' AND eeUploadDirCheck()) {
		include($eePluginPath . '/ee-upload-display.php');
	}
	if(!$eeAdmin AND $eeAllowList == 'Yes') {
		include($eePluginPath . '/ee-list-display.php');
	}
	
	// Admin Display
	if($eeAdmin) {
		echo '<h1>' . $eeMenuLabel . '</h1>';
		include($eePluginPath . '/ee-upload-display.php');
		include($eePluginPath . '/ee-list-display.php');
	}
	
	echo '<div>';
	
	// Write Log to File
	if($eeLogFile) {
		$eeLogFile = $eeUploadDir . '/Simple-File-List-Log.txt';
		
		if(fopen($eeLogFile, "w")) {
			if(is_writable($eeLogFile)) {
			    if(!$handle = fopen($eeLogFile, 'a')) {
			         $eeErrors[] = "Cannot open the log file ($eeLogFile)";
			    } else {
				    
				    foreach($eeLog as $key => $logEntry){
				    
				    	if(@fwrite($handle, '(' . $key . ') ' . $logEntry . "\n") === FALSE) {
					    	$eeErrors[] = "The log file, $eeLogFile had a write failure for: (" . $key . ') ' . $logEntry;
				    	}
				    }
				    	
				    fwrite($handle, "END LOG\n\n\n\nBEGIN LOG");
				
				    fclose($handle);
			    }
			} else {
			    $eeErrors[] = "The log file, $eeLogFile is not writable.";
			}
		}
	}
	
	$eeLog[] = $eeErrors;
	
	if($eeDevMode) { // $eeAdmin AND 
		eeMessageDisplay($eeLog);
	}	
} // ENDs Main Page Display



// Admin Only Admin Page for Administrators
function ee_admin_page_display() {

	eeCheckUser();
	global $wpdb, $eeLog, $eeDevMode, $eeErrors, $eeMessages, $eeMenuLabel, $eeUploadLink, 
		$eePluginPath, $ee_post_max_size, $eeFormats, $eeAdmin, $eeAdminTo, $eeDisclaimer, $eeBackLink, $eeBackLinkTitle;
	
	$eeConfirm = FALSE;
	
	// Create a nonce to check on the included pages.
	$nonce = wp_create_nonce('ee_list_settings');
	
	echo '<h1>Simple File List Settings</h1>';	
	
	include($eePluginPath . '/ee-list-settings.php');
	
	if($eeAdmin AND $eeDevMode) {
		eeMessageDisplay($eeLog);
	}
}
?>